//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Daniel Scroggins on 2/13/22.
//

import UIKit

class StudySetViewController: UIViewController {

    @IBOutlet weak var missedButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var correctButton: UIButton!
    @IBOutlet weak var cardView1: UIView!
    @IBOutlet weak var cardView2: UIView!
    @IBOutlet weak var cardView3: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var missedLabel: UILabel!
    @IBOutlet weak var correctLabel: UILabel!
    @IBOutlet weak var counter1: UILabel!

    @IBOutlet weak var counter2: UILabel!
    
    var cards = Flashcard.getHardCodedCollection()
    
    var flipped = false
    
    var missed = 0
    var correct = 0
    var counterCorrect = 0
    var initialSize: Int = 0


    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSize = cards.count
        cardLabel.text = cards[0].term
        missedLabel.text = "Missed: " + String(missed)
        correctLabel.text = "Correct: " + String(correct)
        counter1.text = String(0)
        counter2.text = String(initialSize)
        

        // Do any additional setup after loading the view.
        let flipCard = UITapGestureRecognizer(target: self, action: #selector(viewTapGesture))
        flipCard.numberOfTouchesRequired = 1
        cardView3.addGestureRecognizer(flipCard)
        makeItPretty()
    }
    
    @objc func viewTapGesture(gesture: UITapGestureRecognizer){
        if(!flipped) {
            cardLabel.text = cards[0].definition
            flipped = true
        }
        else {
            cardLabel.text = cards[0].term
            flipped = false
        }
        mainView.reloadInputViews()
        
    }
    
    @IBAction func missedPressed(_ sender: Any) {

        //if unsolved cards remain
        if ((counterCorrect < initialSize) && (cards.count > 1)) {
            //counting missed
            cards[0].missed = true
            missed += 1
            missedLabel.text = "Missed: " + String(missed)
            //removing and adding the missed card to the end of array
            let missedCard = cards[0]
            cards.remove(at: 0)
            cards.append(missedCard)
            //reseting top card
            flipped = false
            cardLabel.text = cards[0].term
        }
        //otherwise, flash set completed
    }
    @IBAction func skipPressed(_ sender: Any) {
        //if unsolved cards remain
        if ((counterCorrect < initialSize) && (cards.count > 1)) {
            //removing and adding the skipped card to the end of array
            let skippedCard = cards[0]
            cards.remove(at: 0)
            cards.append(skippedCard)
            //reseting top card
            flipped = false
            cardLabel.text = cards[0].term
        }
        //otherwise, flash set completed
    }
    @IBAction func correctPressed(_ sender: Any) {
        //if unsolved cards remain and card has not been missed
        if ((counterCorrect < initialSize) && (cards.count > 1) && (cards[0].missed == false)) {
            //removing correct card
            cards.remove(at: 0)
            //counting hit
            correct += 1
            counterCorrect += 1
            correctLabel.text = "Correct: " + String(correct)
            counter1.text = String(counterCorrect)
            //refreshing data
            flipped = false
            cardLabel.text = cards[0].term
        }
        //if unsolved cards remain and card has been missed
        else if (cards[0].missed == true) {
            //moving card at front to back
            cards[0].missed = false
            let unmissedCard = cards[0]
            cards.remove(at: 0)
            cards.append(unmissedCard)
            //refreshing data
            flipped = false
            cardLabel.text = cards[0].term
        }
        //otherwise, flash set completed
        else {
            let junkCard = Flashcard()
            junkCard.term = "Set Completed"
            junkCard.definition = "Set Completed"
            cards.append(junkCard)
            cards.remove(at: 0)
            correctLabel.text = "Correct: " + String(initialSize)
            counter1.text = String(initialSize)
            flipped = false
            cardLabel.text = cards[0].term
        }


        
    }
    
    
    func makeItPretty()
    {
        //cards
        cardView1.layer.cornerRadius = 8.0
        cardView2.layer.cornerRadius = 8.0
        cardView3.layer.cornerRadius = 8.0
        //buttons
        buttonView.layer.cornerRadius = 8.0
        missedButton.layer.cornerRadius = 8.0
        missedButton.layer.borderColor = UIColor.systemRed.cgColor
        skipButton.layer.cornerRadius = 8.0
        skipButton.layer.borderColor = UIColor.link.cgColor
        correctButton.layer.cornerRadius = 8.0
        correctButton.layer.borderColor = UIColor.systemGreen.cgColor
    }

}
