//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var cards: [Flashcard] = [Flashcard]()
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    

    @IBAction func studyButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "StudySetSegue", sender: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        cards = Flashcard.getHardCodedCollection()
        tableView.delegate = self
        tableView.dataSource = self
        makeItPretty()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressGesture))
        longPress.minimumPressDuration = 1
        longPress.numberOfTouchesRequired = 1
        tableView.addGestureRecognizer(longPress)

        
        
    }
    
    @objc func longPressGesture( gesture:UILongPressGestureRecognizer) {
        if (gesture.state == .began) {
            let touchPoint = gesture.location(in: tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint){
                editCard(indexPath: indexPath)
            }
        }
    }
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let newCard = Flashcard()
        newCard.term = "Term \(cards.count + 1)"
        newCard.definition = "Definition \(cards.count + 1)"
        cards.append(newCard)
        tableView.reloadData()
        tableView.scrollToRow(at: IndexPath(item: tableView.numberOfRows(inSection: tableView.numberOfSections - 1) - 1, section: tableView.numberOfSections - 1), at: .bottom, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alertTitle="Flashcard Contents"
        let message = """
        Term: \(cards[indexPath.row].term)
        Definition: \(cards[indexPath.row].definition)
        """
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            alert.dismiss(animated: true, completion: nil)
            self.editCard(indexPath: indexPath)
        }))
        alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: nil))
        
        
        present(alert, animated: true, completion: nil)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func editCard(indexPath: IndexPath) {
        let term = cards[indexPath.row].term
        let def =  cards[indexPath.row].definition
        let message = "Enter a new term and definition for Flashcard containing " + term + " - " + def
        let alert = UIAlertController(title: "Edit Flashcard", message: message, preferredStyle: .alert)
        
        //add 2 fields
        //Term
        alert.addTextField(configurationHandler: { field in
            field.placeholder = term
            field.returnKeyType = .next
            field.keyboardType = .default
        })
        //Definition
        alert.addTextField(configurationHandler: { field in
            field.placeholder = def
            field.returnKeyType = .continue
            field.keyboardType = .default
        })
        
        //add 2 buttons
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {_ in
            self.cards.remove(at: indexPath.row)
            self.tableView.reloadData()
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { _ in
           //read textfields
            guard let fields = alert.textFields, fields.count == 2 else {
                return
            }
            let termField = fields[0]
            let defField = fields[1]
            guard let newTerm = termField.text, !newTerm.isEmpty,
                  let newDef = defField.text, !newDef.isEmpty else {
                      print("Invalid entries")
                      return
                  }
            self.cards[indexPath.row].term = newTerm
            self.cards[indexPath.row].definition = newDef
            self.tableView.reloadData()
            alert.dismiss(animated: true, completion: nil)
        }))
        
        present(alert, animated: true)
        
    }
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
    
}
